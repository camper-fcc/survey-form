[fCC Build a Survey Form Project](https://beta.freecodecamp.org/en/challenges/applied-responsive-web-design-projects/build-a-survey-form)

[Project](https://camper-fcc.gitlab.io/survey-form/)

[Code](https://gitlab.com/camper-fcc/survey-form) 

[fCC Forum Post](https://www.freecodecamp.org/forum/t/survey-form-material-design-utility-first-css-review-requested/173767?u=camper) 

Here are some details of my project:

- Fonts based on [Unica One](https://fonts.google.com/specimen/Unica+One)<s>, [Vollkorn](https://fonts.google.com/specimen/Vollkorn)</s> combination from [Aesop's Fables in Google Fonts](https://femmebot.github.io/google-type/)
- Color pallete based on [Indigo Pink Material Pallete](https://www.materialpalette.com/indigo/pink)
- Utility-First (Atomic) CSS structure based on [In Defense of Utility-First CSS](https://medium.freecodecamp.org/in-defense-of-utility-first-css-4f406acee6fb) blog post. _(Not sure if I'll continue doing my CSS like this, but it was fun trying it out.)_ :smile:
- CSS Grid and Flexbox for fully responsive, mobile-first design -- **no media queries!** :fireworks:
- <s>Fixed header</s>
- [No inline styles](https://www.thoughtco.com/avoid-inline-styles-for-css-3466846)
- [No CSS ID selectors for styling](http://oli.jp/2011/ids/)
- Uses [semantic HTML](https://internetingishard.com/html-and-css/semantic-html/)
- Passes [FCC Beta tests](https://beta.freecodecamp.org/en/challenges/applied-responsive-web-design-projects/build-a-survey-form)
- Hosted on [GitLab Pages](https://about.gitlab.com/features/pages/)
- Includes Favicon
- Uses [Normalize.css](https://necolas.github.io/normalize.css/)
- Valid [HTML](https://validator.w3.org/nu/?showsource=yes&showoutline=yes&doc=https%3A%2F%2Fcamper-fcc.gitlab.io%2Fsurvey-form%2F), [CSS](https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fcamper-fcc.gitlab.io%2Fsurvey-form%2F&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=en), and [WAVE](http://wave.webaim.org/report#/https://camper-fcc.gitlab.io/survey-form/)

Thank you for your time and input! :sunny: